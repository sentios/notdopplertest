﻿using UnityEngine;

public struct FoodItemData
{
    public string m_FoodName;
    public string m_FoodImageCode;
}
