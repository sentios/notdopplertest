﻿using UnityEngine;

[CreateAssetMenu(fileName = "MockData", menuName = "ScriptableObjects/MockServerData", order = 1)]
public class MockWebData : ScriptableObject
{
    public MockFoodData[] m_Food;

    [System.Serializable]
    public struct MockFoodData
    {
        public string m_FoodName;
        public string m_FoodCode;
        public Sprite m_FoodSprite;
    }
}
