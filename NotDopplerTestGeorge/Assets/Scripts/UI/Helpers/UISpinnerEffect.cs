﻿using UnityEngine;

public class UISpinnerEffect : MonoBehaviour
{
    //-----------------------------------------------------------

    private const float SPINNER_SPEED = 180f;

    //-----------------------------------------------------------

    private void Update()
    {
        transform.Rotate(new Vector3(0f ,0f , SPINNER_SPEED * Time.deltaTime));
    }
}
