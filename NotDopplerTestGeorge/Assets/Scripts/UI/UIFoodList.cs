﻿using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class UIFoodList : MonoBehaviour
{
    private bool m_LoadingItems;

    private List<UIFoodListItem> m_FoodList = new List<UIFoodListItem>();

    //-----------------------------------------------------------

    [SerializeField] private UIFoodListItem m_FoodItemPrefab;
    [SerializeField] private ScrollRect m_ScrollView;
    [SerializeField] private RectTransform m_ViewPortTransform;
    [SerializeField] private RectTransform m_FoodListContent;
    [SerializeField] private RectTransform m_LoadingSpinner;

    [SerializeField] private int m_FoodRequestAmount;

    //-----------------------------------------------------------

    public RectTransform ViewPortRect { get { return m_ViewPortTransform; } }

    //-----------------------------------------------------------

    private void Start()
    {
        m_LoadingItems = true;
        SetLoadingSpinnerState(true);
        WebRequestHelper.RequestFoodData(7, OnFoodDataReceived); //Init enough to be scrollable
        m_ScrollView.onValueChanged.AddListener(OnScrollViewMoved);
    }

    //-----------------------------------------------------------

    public void AddFoodItem(FoodItemData[] a_FoodItems)
    {
        foreach(FoodItemData _Food in a_FoodItems)
        {
            AddFoodItem(_Food);
        }
    }

    //-----------------------------------------------------------

    public void AddFoodItem(FoodItemData a_FoodItem)
    {
        UIFoodListItem _NewFoodItem = Instantiate(m_FoodItemPrefab);
        _NewFoodItem.transform.SetParent(m_FoodListContent.transform);
        _NewFoodItem.transform.localScale = Vector3.one;
        _NewFoodItem.Init(this, a_FoodItem);
        m_FoodList.Add(_NewFoodItem);
    }

    //-----------------------------------------------------------

    private void OnScrollViewMoved(Vector2 a_Value)
    {
        if(!m_LoadingItems)
        {
            //Was checking the vector value but once it gets too long checking if its below 0.1 is unreliable.
            if (m_FoodList.Count > 0 && m_FoodList[m_FoodList.Count-1].IsVisible())
            {
                m_LoadingItems = true;
                SetLoadingSpinnerState(true);
                WebRequestHelper.RequestFoodData(m_FoodRequestAmount, OnFoodDataReceived);
            }
        }
    }

    //-----------------------------------------------------------

    private void SetLoadingSpinnerState(bool a_Enable)
    {
        m_LoadingSpinner.transform.SetAsLastSibling();
        m_LoadingSpinner.gameObject.SetActive(a_Enable);
    }

    //-----------------------------------------------------------

    private void OnFoodDataReceived(WebResponse a_Response)
    {
        m_LoadingItems = false;
        SetLoadingSpinnerState(false);
        AddFoodItem((a_Response as FoodDataResponse).m_FoodData);
    }

    //-----------------------------------------------------------

    private void Update()
    {
    }
}
