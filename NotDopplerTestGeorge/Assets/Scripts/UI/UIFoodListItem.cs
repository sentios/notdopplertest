﻿using UnityEngine;
using UnityEngine.UI;

public class UIFoodListItem : MonoBehaviour
{
    //-----------------------------------------------------------

    private FoodItemData m_Data;
    private UIFoodList m_Controller;
    private bool m_ImageRequested;

    //-----------------------------------------------------------

    [SerializeField] private Image m_LoadingSpinner;
    [SerializeField] private Image m_MainImage;
    [SerializeField] private Text m_Text;

    //-----------------------------------------------------------

    public void Init(UIFoodList a_Controller, FoodItemData a_Data)
    {
        m_Controller = a_Controller;
        m_Data = a_Data;
        m_Text.text = a_Data.m_FoodName;
    }

    //-----------------------------------------------------------

    private void LoadImage()
    {
        WebRequestHelper.RequestFoodImage(m_Data.m_FoodImageCode, OnImageLoaded);
    }

    //-----------------------------------------------------------

    private void OnImageLoaded(WebResponse a_Response)
    {
        m_LoadingSpinner.gameObject.SetActive(false);
        m_MainImage.gameObject.SetActive(true);
        m_MainImage.sprite = (a_Response as FoodImageResponse).m_Sprite;
    }

    //-----------------------------------------------------------

    public bool IsVisible()
    {
        if (transform.position.y >= m_Controller.ViewPortRect.rect.position.y)
        {
            return true;
        }

        return false;
    }

    //-----------------------------------------------------------

    private void Update()
    {
        if(IsVisible() && !m_ImageRequested)
        {
            m_ImageRequested = true;
            LoadImage();
        }
    }

    //-----------------------------------------------------------

}
