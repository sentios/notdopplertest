﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class MockWebServer : MonoBehaviour
{
    //-----------------------------------------------------------

    private static List<PendingResponse> m_PendingResponses = new List<PendingResponse>();
    private static Queue<PendingResponse> m_SendOnMainThread = new Queue<PendingResponse>();
    private Thread m_MockWebServerThread;
    private static MockWebData m_Data;

    //-----------------------------------------------------------

    [SerializeField] private int m_DelayMilliseconds;
    [SerializeField] private MockWebData m_MockData;

    //-----------------------------------------------------------

    private void Awake()
    {
        m_Data = m_MockData;
        m_MockWebServerThread = new Thread(MockWebServerRoutine);
        m_MockWebServerThread.Start();
    }

    //-----------------------------------------------------------

    private void OnDestroy()
    {
        m_MockWebServerThread.Abort();
    }

    //-----------------------------------------------------------

    public static void GETFoodData(FoodDataRequest a_Request)
    {
        FoodItemData[] _FoodData = new FoodItemData[a_Request.m_Amount];
        for(int i = 0; i < _FoodData.Length; i++)
        {
            MockWebData.MockFoodData _Food = m_Data.m_Food[UnityEngine.Random.Range(0, m_Data.m_Food.Length)];
            _FoodData[i] = new FoodItemData() { m_FoodName = _Food.m_FoodName, m_FoodImageCode = _Food.m_FoodCode };
        }

        FoodDataResponse _Response = new FoodDataResponse() { m_FoodData = _FoodData };

        m_PendingResponses.Add(new PendingResponse() { m_Response = _Response, m_Callback = a_Request.m_Callback });
    }

    //-----------------------------------------------------------

    public static void GETFoodImage(FoodImageRequest a_Request)
    {
        for(int i = 0; i < m_Data.m_Food.Length; i++)
        {
            if(m_Data.m_Food[i].m_FoodCode == a_Request.m_ImageCode)
            {
                FoodImageResponse _Response = new FoodImageResponse() { m_Sprite = m_Data.m_Food[i].m_FoodSprite };

                m_PendingResponses.Add(new PendingResponse() { m_Response = _Response, m_Callback = a_Request.m_Callback });

                break;
            }
        }
    }

    //-----------------------------------------------------------

    private void MockWebServerRoutine()
    {
        while(true)
        {
            for(int i = m_PendingResponses.Count-1; i >= 0 ; i--)
            {
                m_PendingResponses[i].m_TimePassed += 20f;

                if(m_PendingResponses[i].m_TimePassed >= m_DelayMilliseconds)
                {
                    m_SendOnMainThread.Enqueue(m_PendingResponses[i]);
                    m_PendingResponses.RemoveAt(i);
                }
            }

            Thread.Sleep(20);
        }
    }

    //-----------------------------------------------------------

    private void Update()
    {
        if(m_SendOnMainThread.Count > 0)
        {
            PendingResponse _PendingResponse = m_SendOnMainThread.Dequeue();
            _PendingResponse.m_Callback.Invoke(_PendingResponse.m_Response);
        }
    }

    //-----------------------------------------------------------

    private class PendingResponse
    {
        public float m_TimePassed;
        public WebResponse m_Response;
        public Action<WebResponse> m_Callback;
    }

    //-----------------------------------------------------------

}
