﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class WebRequestHelper
{
    public static void RequestFoodData(int a_Amount, Action<WebResponse> a_Callback)
    {
        MockWebServer.GETFoodData(new FoodDataRequest() { m_Amount = a_Amount, m_Callback = a_Callback });
    }

    public static void RequestFoodImage(string m_ImageCode, Action<WebResponse> a_Callback)
    {
        MockWebServer.GETFoodImage(new FoodImageRequest() { m_ImageCode = m_ImageCode, m_Callback = a_Callback });
    }
}
